package ivan.smolin.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.qameta.allure.Allure;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import java.net.URI;

import static org.springframework.http.HttpMethod.GET;

/**
 * Basic service class to send requests to a specific API service
 */
@Slf4j
public abstract class BaseService {

    @Autowired
    private TestRestTemplate testRestTemplate;

    /**
     * This block of methods is responsible for sending GET requests
     */
    protected <T> ResponseEntity<T> getRequest(URI url, Class<T> responseType) {
        return addReportDescription(
                GET.name(),
                url.toString(),
                null,
                testRestTemplate.getForEntity(url, responseType)
        );
    }

    /**
     * The method writes a description to the Allure report for each test
     * The description contains the URL used, the request body, if there was one, the status code and
     * the response body, if there was one
     *
     * @param restType       REST request type
     * @param url            The URL where the request is made
     * @param requestBody    request body
     * @param responseEntity response body with statuses and headers
     * @param <T>            response body type
     * @return responseEntity
     */
    @SneakyThrows
    private <T> ResponseEntity<T> addReportDescription(
            String restType,
            String url,
            Object requestBody,
            ResponseEntity<T> responseEntity
    ) {
        ObjectWriter writer = new ObjectMapper().writerWithDefaultPrettyPrinter();

        String description = "<b>Используемый URL:</b> " + restType + "  " + url;

        description += requestBody != null
                ? "<br><br><b>Request Body:</b><pre>" + writer.writeValueAsString(requestBody) + "</pre>"
                : "";

        description += "<br><br><b>Response Body:</b>" +
                "<br><i><b>Status Code:</b></i> " + responseEntity.getStatusCode();

        description += responseEntity.hasBody()
                ? "<br><i><b>Body:</b></i><pre>" + writer.writeValueAsString(responseEntity.getBody()) + "</pre>"
                : "";

        description += "<br><i><b>Headers:</b></i><pre>" +
                writer.writeValueAsString(responseEntity.getHeaders()) + "</pre>";

        Allure.descriptionHtml(description);

        log.info(
                description.replaceAll("<b>|</b>|</i>|<i>|<pre>|</pre>", "")
                        .replaceAll("<br>", "\n")
        );

        return responseEntity;
    }
}