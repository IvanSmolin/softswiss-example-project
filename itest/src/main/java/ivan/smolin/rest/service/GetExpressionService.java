package ivan.smolin.rest.service;

import ivan.smolin.service.BaseService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

/**
 * Service for GET api.mathjs.org/v4?expr={expression} API method
 */
@Service
public final class GetExpressionService extends BaseService {

    @Value("${app.mathjs.expression}")
    private String expressionUrl;

    /**
     * The method executes GET request at a specific URL
     *
     * @param firstArgument  first argument of expression
     * @param sign           arithmetic sign of expression
     * @param secondArgument second argument of expression
     * @return expression response
     */
    @SneakyThrows
    public ResponseEntity<String> get(double firstArgument, String sign, double secondArgument) {
        return getRequest(
                new URI(buildUrl(firstArgument, sign, secondArgument)),
                String.class
        );
    }

    private String buildUrl(double firstArgument, String sign, double secondArgument) {
        return UriComponentsBuilder.fromHttpUrl(expressionUrl)
                .buildAndExpand((firstArgument + sign + secondArgument)
                        .replace("+", "%2B")).toString();
    }
}