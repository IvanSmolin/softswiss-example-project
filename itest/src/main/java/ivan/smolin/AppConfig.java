package ivan.smolin;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;

/**
 * The main class for tests
 */
@SpringBootApplication
public class AppConfig {

    /**
     * Creating a bean for TestRestTemplate when raising the spring context
     *
     * @return initialized TestRestTemplate
     */
    @Bean
    public TestRestTemplate testRestTemplate() {
        return new TestRestTemplate();
    }
}