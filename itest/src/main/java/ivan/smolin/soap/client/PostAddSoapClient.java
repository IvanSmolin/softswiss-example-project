package ivan.smolin.soap.client;

import ivan.smolin.client.BaseClient;
import ivan.smolin.soap.service.wsdl.Add;
import ivan.smolin.soap.service.wsdl.AddResponse;
import org.springframework.beans.factory.annotation.Value;

/**
 * Client for POST "/Add" API method
 */
public class PostAddSoapClient extends BaseClient {

    @Value("${app.dneonline.add}")
    private String actionCallback;

    @Value("${app.dneonline.wsdl}")
    private String url;

    public AddResponse postAdd(int a, int b) {
        Add request = new Add();
        request.setIntA(a);
        request.setIntB(b);

        return (AddResponse) postRequest(url, request, actionCallback);
    }
}