package ivan.smolin.soap.client;

import ivan.smolin.client.BaseClient;
import ivan.smolin.soap.service.wsdl.Divide;
import ivan.smolin.soap.service.wsdl.DivideResponse;
import org.springframework.beans.factory.annotation.Value;

/**
 * Client for POST "/Divide" API method
 */
public class PostDivideSoapClient extends BaseClient {

    @Value("${app.dneonline.divide}")
    private String actionCallback;

    @Value("${app.dneonline.wsdl}")
    private String url;

    public DivideResponse postDivide(int a, int b) {
        Divide request = new Divide();
        request.setIntA(a);
        request.setIntB(b);

        return (DivideResponse) postRequest(url, request, actionCallback);
    }
}