package ivan.smolin.soap.configuration;

import ivan.smolin.soap.client.PostAddSoapClient;
import ivan.smolin.soap.client.PostDivideSoapClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class SoapCalculatorConfig {

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("ivan.smolin.soap.service.wsdl");

        return marshaller;
    }

    @Bean
    public PostAddSoapClient postAddSoapClient(Jaxb2Marshaller marshaller) {
        PostAddSoapClient client = new PostAddSoapClient();
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);

        return client;
    }

    @Bean
    public PostDivideSoapClient postDivideSoapClient(Jaxb2Marshaller marshaller) {
        PostDivideSoapClient client = new PostDivideSoapClient();
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);

        return client;
    }
}