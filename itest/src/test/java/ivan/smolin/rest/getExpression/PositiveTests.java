package ivan.smolin.rest.getExpression;

import ivan.smolin.BaseTestClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.ResponseEntity;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.http.HttpStatus.OK;

@DisplayName("API. REST Service. GET '/?expr={expression}'. Позитивные тесты сложения и деления двух чисел")
public final class PositiveTests extends BaseTestClass {

    @ParameterizedTest(name = "Успешное выполнение арифметического выражения: {0} {1} {2} = {3}")
    @MethodSource(value = "getVariables")
    public void testGetExpressionSuccess(double firstArgument, String sign, double secondArgument, String answer) {
        ResponseEntity<String> responseEntity = getExpressionService.get(firstArgument, sign, secondArgument);

        assertOkResponse(responseEntity, answer);
    }

    private static Stream<Arguments> getVariables() {
        return Stream.of(
                Arguments.of(0, "+", -1, "-1"),
                Arguments.of(2.5, "+", 3.5, "6"),
                Arguments.of(2, "+", 2, "4"),
                Arguments.of(1, "/", 789, "0.0012674271229404308"),
                Arguments.of(1, "/", 1, "1"),
                Arguments.of(3.5, "/", 2, "1.75")
        );
    }

    private void assertOkResponse(ResponseEntity<String> responseEntity, String answer) {
        assertAll(
                () -> assertThat(responseEntity.getStatusCode()).isEqualTo(OK),
                () -> assertThat(responseEntity.getBody()).isEqualTo(answer)
        );
    }
}