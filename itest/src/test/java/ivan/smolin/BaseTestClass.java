package ivan.smolin;

import ivan.smolin.rest.service.GetExpressionService;
import ivan.smolin.soap.client.PostAddSoapClient;
import ivan.smolin.soap.client.PostDivideSoapClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public abstract class BaseTestClass {

    @Autowired
    protected GetExpressionService getExpressionService;

    @Autowired
    protected PostAddSoapClient postAddSoapClient;

    @Autowired
    protected PostDivideSoapClient postDivideSoapClient;
}