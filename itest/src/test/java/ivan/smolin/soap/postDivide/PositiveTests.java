package ivan.smolin.soap.postDivide;

import ivan.smolin.BaseTestClass;
import ivan.smolin.soap.service.wsdl.AddResponse;
import ivan.smolin.soap.service.wsdl.DivideResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("API. SOAP Service. POST '/Divide'. Позитивные тесты деления двух чисел")
public final class PositiveTests extends BaseTestClass {

    @ParameterizedTest(name = "Успешное выполнение арифметического выражения: {0} / {1} = {2}")
    @MethodSource(value = "getVariables")
    public void testPostAddSuccess(int firstArgument, int secondArgument, int answer) {
        DivideResponse response = postDivideSoapClient.postDivide(firstArgument, secondArgument);

        assertEquals(answer, response.getDivideResult());
    }

    private static Stream<Arguments> getVariables() {
        return Stream.of(
                Arguments.of(3, 2, 2),
                Arguments.of(1, 1, 1),
                Arguments.of(100, 10, 10)
        );
    }
}