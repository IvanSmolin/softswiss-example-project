package ivan.smolin.soap.postAdd;

import ivan.smolin.BaseTestClass;
import ivan.smolin.soap.service.wsdl.AddResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("API. SOAP Service. POST '/Add'. Позитивные тесты сложения двух чисел")
public final class PositiveTests extends BaseTestClass {

    @ParameterizedTest(name = "Успешное выполнение арифметического выражения: {0} + {1} = {2}")
    @MethodSource(value = "getVariables")
    public void testPostAddSuccess(int firstArgument, int secondArgument, int answer) {
        AddResponse response = postAddSoapClient.postAdd(firstArgument, secondArgument);

        assertEquals(answer, response.getAddResult());
    }

    private static Stream<Arguments> getVariables() {
        return Stream.of(
                Arguments.of(1, 789, 790),
                Arguments.of(1, 1, 2),
                Arguments.of(0, -1, -1),
                Arguments.of(2, 2, 4)
        );
    }
}