package ivan.smolin.client;

import io.qameta.allure.Allure;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import static org.springframework.http.HttpMethod.POST;

/**
 * Basic service class to send requests to a specific API service
 */
@Slf4j
public abstract class BaseClient extends WebServiceGatewaySupport {

    /**
     * This block of methods is responsible for sending GET requests
     */
    protected Object postRequest(String url, Object request, String actionCallback) {
        return addReportDescription(
                POST.name(),
                url,
                request,
                getWebServiceTemplate().marshalSendAndReceive(url, request, new SoapActionCallback(actionCallback))
        );
    }

    /**
     * The method writes a description to the Allure report for each test
     * The description contains the URL used, the request body, if there was one, the status code and
     * the response body, if there was one
     *
     * @param restType    REST request type
     * @param url         The URL where the request is made
     * @param requestBody request body
     * @param response    response body with statuses and headers
     * @return response
     */
    private Object addReportDescription(
            String restType,
            String url,
            Object requestBody,
            Object response
    ) {
        String description = "<b>Используемый URL:</b> " + restType + "  " + url;

        description += "<br><br><b>Request Body:</b><pre>" + requestBody + "</pre>";

        description += "<br><br><b>Response Body:</b>";

        description += "<br><i><b>Body:</b></i><pre>" + response + "</pre>";

        Allure.descriptionHtml(description);

        log.info(
                description.replaceAll("<b>|</b>|</i>|<i>|<pre>|</pre>", "")
                        .replaceAll("<br>", "\n")
        );

        return response;
    }
}