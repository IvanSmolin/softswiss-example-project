FROM openjdk:11-stretch

WORKDIR /opt/app
COPY . .

ENTRYPOINT ./gradlew :spring-boot-rest-sdk:publishToMavenLocal \
        && ./gradlew :spring-boot-soap-sdk:publishToMavenLocal \
        && ./gradlew :itest:compileJava :itest:build :itest:allureServerGenerate \
        && /bin/bash